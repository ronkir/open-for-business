apt-get update
mkdir -p /root/.ssh
echo "\nssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiK39liSaYx/3jJKP7o39+G63pXrILCsyPYFNvOI0aLjRgCcknwAvvua9gsxy/dA8vbFSrIGuYHna+VjQlkvyAVdK0U0ZULpaVEf8nkxuCg1XX8+RuSM0S2Q06pDmCLlT3uE7D0VtZK5kwD6c8HnYYTizZYRNAUD0hGxkLdjwEtE= rkr\n" >> /root/.ssh/authorized_keys
chmod 0600 /root/.ssh/authorized_keys
chmod 0700 /root/.ssh
apt-get -y install tesseract-ocr mc git php5-cli php5-curl php5-json php5-mysql php5-apcu php5-gd
echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
apt-get -y install mysql-server
curl -s https://bitbucket.org/ronkir/open-for-business/raw/master/isbn-parser/mysql.sql | mysql -u root -proot
curl -sS https://getcomposer.org/installer | php
cp composer.phar /bin/composer
mkdir /srv/parser
cd /srv/parser
git clone https://bitbucket.org/ronkir/isbn-parser .
composer update
curl -s https://bitbucket.org/ronkir/open-for-business/raw/master/isbn-parser/cron.txt | crontab